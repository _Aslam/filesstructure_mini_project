print("----------------------------------------------------------------------------------------")
print("| ------------------------------------------------------------------------------------ |")
print("| |                       _____            _____    ____             _____           | |")
print("| |            |      |  |        |       (        (    )  |\    /| |                | |")
print("| |            |  /\  |  |_____   |       |        |    |  | \  / | |____            | |")
print("| |            | /  \ |  |        |       |        |    |  |  \/  | |                | |")
print("| |            |/    \|  |_____   |_____  (_____   (____)  |      | |_____           | |")
print("| ------------------------------------------------------------------------------------ |")
print("----------------------------------------------------------------------------------------")
print("\n\n")
flag=0
while(1):
	print("----------------------------------------------------------------------------------------")
	print("|                                  MAIN MENU                                           |")
	print("----------------------------------------------------------------------------------------")
	
	print("|Select any one of the given options:                                                  |")
	print("|1. Staff.                                                                             |")
	print("|2. Animals.                                                                           |")
	print("|3. Tourist's.                                                                         |")
	print("|4. Inventory.                                                                         |")
	print("|5. Exit...                                                                            |")
	ch=int(input(' Enter your choice:'))
	if ch==1:
		while(1):
			print("----------------------------------------------------------------------------------------")
			print("                                        STAFF..")
			print("----------------------------------------------------------------------------------------")
			print("1. Insert record.")
			print("2. Display.")
			print("3. search.")
			print("4. Delete.")
			print("5. Go Back..")
			s=int(input('Enter your choice..'))
			if s==1:
				#insert staff
				file1=open("staff.txt","a+")
				ssn=input('Enter unique SSN number: ')
				if(int(len(ssn))>10):
					print('======invalid entry======')
					break
				dob=input('Enter the date of birth (use colan to saperate): ')
				if(int(len(dob))>10) or dob in 'qwertyuiopasdfghjklzxcvbnm':
					print('======invalid entry======')
					break
				department=input('Enter your department: ')
				if int(len(department))>10:
					print('======invalid entry======')
					break
				shift=input('Enter shift (example, morning): ')
				if shift not in 'morningeveningnight':
					print('======invalid entry======')
					break
				gender=input('Enter Gender: ')
				if gender not in 'mail,female,other':
					print('======invalid entry======')
					break
				phno=input('Enter Phone number: ')
				if int(len(phno))>10:
					print('======invalid entry======')
					break
				salary=input('Enter salary: ')
				if(int(len(salary))>10):
					print('======invalid entry======')
					break
				for i in salary:
					if i not in '0123456789':
						print('======invalid entry======')
						flag=1
						break
				if flag==1:
					flag=0
					break
				addr=input('Enter Address: ')
				if (int(len(addr))>10):
					print('======invalid entry======')
					break
				final=ssn+"|"+dob+"|"+department+"|"+shift+"|"+gender+"|"+phno+"|"+salary+"|"+addr+"\n"
				file1.write(final)
				file1.close()
				print("successfully inserted new record")
				print("----------------------------------------------------------------------------------------")

			elif s==2:
				#final display for staff
				f=open("staff.txt","r")
				c=f.read()
				j=0
				print("------------------------------------------------------------------------------------------------")
				print("|SSN"+" "*8+"|DOB"+" "*8+"|Department"+" "+"|Shift"+" "*6+"|gender"+" "*5+"|phno"+" "*7+"|salary"+" "*5+"|Address"+" "*3+"|")
				print("------------------------------------------------------------------------------------------------")				
				for i in c:
					j+=1
					if j==1:
						print("|",end="")
					if(i=='|'):
						d=12-j
						j=0
						print(" "*d,end="")
					else:
						if(i=='\n'):
							d=11-j
							print(" "*d+"|",end="")
							j=0
						print(i,end="")

				f.close()
	
			elif s==3:
				#search for staff record
				f=open("staff.txt","r")
				c=f.read()
				key=input("enter the key to be searched in the file: ")
				w=""
				value=""
				print("-------------------------------------------------------------------------------------------------")
				print("|SSN"+" "*8+"|DOB"+" "*8+"|Department"+" "+"|Shift"+" "*6+"|gender"+" "*5+"|phno"+" "*7+"|salary"+" "*5+"|Address"+" "*4+"|")
				print("-------------------------------------------------------------------------------------------------")
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							for j in w:
								if j=='|' or j=='\n':
									print("|"+value," "*(10-len(value)),end='')
									value=''
									if j=='\n':
										print("|")
								else:
									value=value+j
						w=''
				if key not in c:
					print("| No such record could be found...                                                              |")
				f.close()
	
			elif s==4:
				#delete for staff
				f=open("staff.txt","r")
				c=f.read()
				tc=''
				key=input("enter unique to delete the the entry: ")
				w=""
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							w=''
						tc=tc+w
						w=''
				f.close()
				f=open("staff.txt","w")
				f.write(tc)
				f.close()
			
			elif s==5:
				break
		
			else:
				print("----------------------------------------------------------------------------------------")
				print("                                  Invalid input..")
				print("----------------------------------------------------------------------------------------")
			
	elif ch==2:
		while(1):
			print("----------------------------------------------------------------------------------------")
			print("                                        ANIMAL..")
			print("----------------------------------------------------------------------------------------")
			print("1. Insert record.")
			print("2. Display.")
			print("3. search.")
			print("4. Delete.")
			print("5.Go Back.")
			s=int(input('Enter your choice..'))
			if s==1:
				#insert Animal
				file1=open("animal.txt","a+")
				aid=input('Enter unique aid number: ')
				if (int(len(aid))>10):
					print('======invalid entry======')
					break
				name=input('Enter Animal name: ')
				if (int(len(name))>10):
					print('======invalid entry======')
					break
				for i in name:
					if i in '0123456789':
						print('======invalid entry======')
						flag=1
						break
				if flag==1:
					flag=0
					break
				age=input('Enter Animal age: ')
				if (int(len(age))>10):
					print('======invalid entry======')
					break
				for i in age:
					if i not in '0123456789':
						print('======invalid entry======')
						flag=1
						break
				if flag==1:
					flag=0
					break
				food=input('Enter food habitate: ')
				if (int(len(food))>10):
					print('======invalid entry======')
					break
				gender=input('Enter Gender: ')
				if (int(len(gender))>10) or gender not in 'male female other':
					print('======invalid entry======')
					break
				Place=input('Enter Place of adaption: ')
				if (int(len(place))>10):
					print('======invalid entry======')
					break
				for i in place:
					if i in '0123456789':
						print('======invalid entry======')
						flag=1
						break
				if flag==1:
					flag=0
					break
				ssid=input('Enter ssid: ')
				if (int(len(ssid))>10):
					print('======invalid entry======')
					break
				health=input('Enter health (good/bad): ')
				if health not in 'good bad awesome excellent critical':
					print('======invalid entry======')
					break
				final=aid+"|"+name+"|"+age+"|"+food+"|"+gender+"|"+Place+"|"+ssid+"|"+health+"\n"
				file1.write(final)
				file1.close()
				print("successfully inserted new record")
	
			elif s==2:
				#final display for animal
				f=open("animal.txt","r")
				c=f.read()
				j=0
				print("------------------------------------------------------------------------------------------------")
				print("|AID"+" "*8+"|Name"+" "*7+"|Age"+" "*8+"|Food"+" "*7+"|Gender"+" "*5+"|Place"+" "*6+"|SSID"+" "*7+"|Helth"+" "*5+"|")
				print("------------------------------------------------------------------------------------------------")				
				for i in c:
					j+=1
					if j==1:
						print("|",end="")
					if(i=='|'):
						d=12-j
						j=0
						print(" "*d,end="")
					else:
						if(i=='\n'):
							d=11-j
							print(" "*d+"|",end="")
							j=0
						print(i,end="")
				f.close()
	
			elif s==3:
				#search for animal record
				f=open("animal.txt","r")
				c=f.read()
				key=input("enter the key to be searched in the file: ")
				w=""
				value=""
				print("-------------------------------------------------------------------------------------------------")
				print("|AID"+" "*8+"|Name"+" "*7+"|Age"+" "*8+"|Food"+" "*7+"|Gender"+" "*5+"|Place"+" "*6+"|SSID"+" "*7+"|Helth"+" "*6+"|")
				print("-------------------------------------------------------------------------------------------------")
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							for j in w:
								if j=='|' or j=='\n':
									print("|"+value," "*(10-len(value)),end='')
									value=''
									if j=='\n':
										print("|")
								else:
									value=value+j
						w=''
				if key not in c:
					print("| No such record could be found...                                                              |")
				f.close()
	
			elif s==4:
				#delete for animal
				f=open("animal.txt","r")
				c=f.read()
				tc=''
				key=input("enter unique to delete the the entry: ")
				w=""
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							w=''
						tc=tc+w
						w=''
				f.close()
				f=open("animal.txt","w")
				f.write(tc)
				f.close()

			elif s==5:
				break
	
			else:
				print("----------------------------------------------------------------------------------------")
				print("                                  Invalid input..")
				print("----------------------------------------------------------------------------------------")
	
	
	elif ch==3:
		while(1):
			print("----------------------------------------------------------------------------------------")
			print("                                        TOURIST..")
			print("----------------------------------------------------------------------------------------")
			print("1. Insert record.")
			print("2. Display.")
			print("3. search.")
			print("4. Delete.")
			print("5. Go Back.")
			s=int(input('Enter your choice..'))
			if s==1:
				#tourist insert
				file1=open("tourist.txt","a+")
				tid=input('Enter unique tid number: ')
				if (int(len(tid))>10) or tid not in '0123456789':
					print('======invalid entry======')
					break
				tno=input('Enter the number of tickets (max ticket=4 and min=1): ')
				if (int(len(tno))>1) or int(tno)>4:
					print('invalid entry')
					break
				name=input('Enter Tourist name: ')
				if (int(len(name))>10) or tid not in '0123456789':
					print('======invalid entry======')
					break
				phno=input('Enter phno: ')
				if (int(len(phno))>10):
					print('======invalid entry=====')
					break
				final=tid+"|"+tno+"|"+name+"|"+phno+"\n"
				file1.write(final)
				file1.close()
				print("successfully inserted new record")
	
			elif s==2:
				#tourist dislay
				f=open("tourist.txt","r")
				c=f.read()
				j=0
				print("------------------------------------------------")
				print("|tid"+" "*8+"|tno"+" "*8+"|name"+" "*7+"|phno"+" "*6+"|")
				print("------------------------------------------------")				
				for i in c:
					j+=1
					if j==1:
						print("|",end="")
					if(i=='|'):
						d=12-j
						j=0
						print(" "*d,end="")
					else:
						if(i=='\n'):
							d=11-j
							print(" "*d+"|",end="")
							j=0
						print(i,end="")
				f.close()
	
			elif s==3:
				#search for tourist record
				f=open("tourist.txt","r")
				c=f.read()
				key=input("enter the key to be searched in the file: ")
				w=""
				value=""
				print("-------------------------------------------------")
				print("|tid"+" "*8+"|tno"+" "*8+"|name"+" "*7+"|phno"+" "*7+"|")
				print("-------------------------------------------------")
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							for j in w:
								if j=='|' or j=='\n':
									print("|"+value," "*(10-len(value)),end='')
									value=''
									if j=='\n':
										print("|")
								else:
									value=value+j
						w=''
				if key not in c:
					print("| No such record could be found...              |")
				f.close()
	
			elif s==4:
				#delete for tourist
				f=open("tourist.txt","r")
				c=f.read()
				tc=''
				key=input("enter unique to delete the the entry: ")
				w=""
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							w=''
						tc=tc+w
						w=''
				f.close()
				f=open("tourist.txt","w")
				f.write(tc)
				f.close()
	
			elif s==5:
				break
	
			else:
				print("----------------------------------------------------------------------------------------")
				print("                                  Invalid input..")
				print("----------------------------------------------------------------------------------------")


	elif ch==4:
		while(1):
			print("----------------------------------------------------------------------------------------")
			print("                                        INVENTORY..")
			print("----------------------------------------------------------------------------------------")
			print("1. Insert record.")
			print("2. Display.")
			print("3. search.")
			print("4. Delete.")
			print("5. Go Back.")
			s=int(input('Enter your choice..'))
			if s==1:
				#insert inventory
				file1=open("inventory.txt","a+")
				lid=input('Enter unique lid number: ')
				if(int(len(lid))>10):
					print('======invalid enter======')
					break
				itno=input('Enter the Item number: ')
				if(int(len(itno))>10):
					print('======invalid enter======')
					break
				name=input('Enter item name: ')
				if(int(len(name))>10):
					print('======incalid enter======')
					break
				quantity=input('Enter quantity: ')
				if(int(len(quantity))>10):
					print('======incalid enter======')
					break
				Type=input('Enter Type: ')
				if(int(len(Type))>10):
					print('======incalid enter======')
					break
				stall_no=input('Enter stall_no of adaption: ')
				if(int(len(stall_no))>10):
					print('======incalid enter======')
					break
				location=input('Enter location: ')
				if(int(len(location))>10):
					print('======incalid enter======')
					break
				final=lid+"|"+itno+"|"+name+"|"+quantity+"|"+Type+"|"+stall_no+"|"+location+"\n"
				file1.write(final)
				file1.close()
				print("successfully inserted new record")
	
			elif s==2:
				#final display for inventory
				f=open("inventory.txt","r")
				c=f.read()
				j=0
				print("------------------------------------------------------------------------------------")
				print("|lid"+" "*8+"|itno"+" "*7+"|name"+" "*7+"|quantity"+" "*3+"|Type"+" "*7+"|stall_no"+" "*3+"|location"+" "*2+"|")
				print("------------------------------------------------------------------------------------")				
				for i in c:
					j+=1
					if j==1:
						print("|",end="")
					if(i=='|'):
						d=12-j
						j=0
						print(" "*d,end="")
					else:
						if(i=='\n'):
							d=11-j
							print(" "*d+"|",end="")
							j=0
						print(i,end="")
				f.close()
	
			elif s==3:
				#search for inventory record
				f=open("inventory.txt","r")
				c=f.read()
				key=input("enter the key to be searched in the file: ")
				w=""
				value=""
				print("-------------------------------------------------------------------------------------")
				print("|lid"+" "*8+"|itno"+" "*7+"|name"+" "*7+"|quantity"+" "*3+"|Type"+" "*7+"|stall_no"+" "*3+"|location"+" "*3+"|")
				print("-------------------------------------------------------------------------------------")
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							for j in w:
								if j=='|' or j=='\n':
									print("|"+value," "*(10-len(value)),end='')
									value=''
									if j=='\n':
										print("|")
								else:
									value=value+j
						w=''
				if key not in c:
					print("| No such record could be found...                                                  |")
				f.close()
	
			elif s==4:
				#delete for inventory
				f=open("inventory.txt","r")
				c=f.read()
				tc=''
				key=input("enter unique to delete the the entry: ")
				w=""
				for i in c:
					w=w+i
					if i=='\n':
						if key in w:
							w=''
						tc=tc+w
						w=''
				f.close()
				f=open("inventory.txt","w")
				f.write(tc)
				f.close()
	
			elif s==5:
				break
	
			else:
				print("----------------------------------------------------------------------------------------")
				print("                                  Invalid input..")
				print("----------------------------------------------------------------------------------------")


	elif ch==5:
		print("----------------------------------------------------------------------------------------")
		print("                                 closing programme.....")
		print("----------------------------------------------------------------------------------------")
		break

	else:
		print("----------------------------------------------------------------------------------------")
		print("                                 Invalid input..")
		print("----------------------------------------------------------------------------------------")

#alpha@re2rl published under gpl v3
